import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.UP) {
            model.getPizzaBox().move(0, -10);
        } else if (key == KeyCode.DOWN) {
            model.getPizzaBox().move(0, 10);
        } else if (key == KeyCode.LEFT) {
            model.getPizzaBox().move(-10, 0);
        } else if (key == KeyCode.RIGHT) {
            model.getPizzaBox().move(10, 0);
        }

    }

    public void onKeyReleased(KeyCode key) {

    }
}
