import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {
    private Model model;
    private Graphics graphics;

    private long previousTime = -1;
    private long spielDauer;

    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long nowNano) {
        long nowMillis = nowNano / 1000000;

        long elapsedTime;

        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMillis - previousTime;
        }
        previousTime = nowMillis;
        spielDauer += elapsedTime;
        model.update(elapsedTime);

        graphics.draw();
        if(spielDauer > 120000){
            model.gameOver(true);
        }
        model.gameOver(false);
        System.out.println(spielDauer);
        model.gameWon();

        if (model.isGameOver() || model.isGameWon()){
            graphics.draw();
            stop();
        }


    }
}
