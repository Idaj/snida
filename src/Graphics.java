import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.*;
import javafx.scene.canvas.GraphicsContext;
import model.Model;

public class Graphics {

    private Model model;
    private GraphicsContext gc;
    Image pizza = new Image("images/pizza.png", 50,50, false, false);
    Image pizzaCutter = new Image("images/pizza-cutter.png", 50,50,false,false);
    Image pizzaBox = new Image ("images/pizzaBox.png", 50,50,false, false);



    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }


    public void  draw (){
        gc.clearRect(0,0,Model.WIDTH, Model.HEIGHT);


        PizzaBox s = model.getPizzaBox();
        gc.drawImage(pizzaBox,
                s.getX() - s.getW() / 2,
                s.getY() - s.getH() / 2,
                s.getW(),
                s.getH()
        );


        for (PizzaCutter pizzaCutter : this.model.getPizzaCutter()) {
            gc.drawImage(this.pizzaCutter,
                    pizzaCutter.getX() - pizzaCutter.getW() / 2,
                    pizzaCutter.getY() - pizzaCutter.getH() / 2,
                    pizzaCutter.getW(),
                    pizzaCutter.getH()
            );
        }

            PizzaSlice f = model.getPizzaSlice();
            gc.drawImage(pizza,
                    f.getX() - f.getW() / 2,
                    f.getY() - f.getH() / 2,
                    f.getW(),
                    f.getH()
            );

            gc.setFont(new Font(25));
            gc.setFill(Color.BLACK);
            gc.fillText("SCORE " + model.getScore(),840,570);


            if(model.isGameOver() == true){
                gc.setFont(new Font(85));
                gc.setFill(Color.BLACK);
                gc.clearRect(0,0,Model.WIDTH, Model.HEIGHT);
                gc.fillText("Was kannst du eigentlich" ,30,300);
            }

            if(model.isGameWon() == true){
                gc.setFont(new Font(95));
                gc.setFill(Color.BLACK);
                gc.clearRect(0,0,Model.WIDTH, Model.HEIGHT);
                gc.fillText("Pizza König und ihr so" ,30,300);
            }
        }


    }

