package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    private List<PizzaCutter> pizzaCutter = new LinkedList<>();
    private PizzaBox pizzaBox;
    private PizzaSlice pizzaSlice;
    private int score = 0;
    private boolean gameOver = false;
    private boolean gameWon = false;
    private boolean kollisionMitEnemy = false;


    public List<PizzaCutter> getPizzaCutter() {
        return pizzaCutter;
    }
    public PizzaBox getPizzaBox() {
        return pizzaBox;
    }
    public PizzaSlice getPizzaSlice() { return pizzaSlice; }

    public Model() {
        Random random = new Random();

        for (int i = 0; i < 8; i++) {
            this.pizzaCutter.add(new PizzaCutter(0, random.nextInt(475)+25, 0.1f + random.nextFloat() * 0.2f)
            );
        }

        this.pizzaBox = new PizzaBox(200, 560);
        this.pizzaSlice = new PizzaSlice(700, 200);
    }

    public void update(long elapsedTime) {
        for (PizzaCutter pizzaCutter : this.pizzaCutter) {
            pizzaCutter.update(elapsedTime);
        }

        for (PizzaCutter pizzaCutter : this.pizzaCutter) {
            int dx = Math.abs(pizzaBox.getX() - pizzaCutter.getX());
            int dy = Math.abs(pizzaBox.getY() - pizzaCutter.getY());
            int w = pizzaBox.getW() / 2 + pizzaCutter.getW() / 2;
            int h = pizzaBox.getH() / 2 + pizzaCutter.getH() / 2;

            if (dx <= w && dy <= h) {
                pizzaBox.moveTo(200, 560);
                score--;
                kollisionMitEnemy = true;
            }
        }
        Random random = new Random();
            int dx = Math.abs(pizzaBox.getX() - pizzaSlice.getX());
            int dy = Math.abs(pizzaBox.getY() - pizzaSlice.getY());
            int w = pizzaBox.getW() / 2 + pizzaSlice.getW() / 2;
            int h = pizzaBox.getH() / 2 + pizzaSlice.getH() / 2;

            if (dx <= w && dy <= h) {
                pizzaSlice.moveTo( random.nextInt(1000),random.nextInt(540));
                score++;
            }
        }

        public void gameOver(boolean status){
            if(status){
                gameOver = true;
            }
            if(this.score <= 0  && kollisionMitEnemy == true){
            gameOver = true;

         }
        }

        public void gameWon() {
        if(this.score == 8){
            gameWon = true;
        }
        }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isGameWon() {
        return gameWon;
    }

    public int getScore() {
        return score;
    }
}

