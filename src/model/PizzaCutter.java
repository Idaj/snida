package model;

public class PizzaCutter {
    private int x;
    private int y;
    private float speed;

    public PizzaCutter(int x, int y, float speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speed);

        if (this.x > Model.WIDTH && this.speed > 0 || this.x < 0 && this.speed < 0) {
            this.speed = -1 * this.speed;
        }
    }



    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 50;
    }

    public int getW() {
        return 50;
    }
}
