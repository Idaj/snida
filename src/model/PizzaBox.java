package model;

public class PizzaBox {
    private int x;
    private int y;
    private int h;
    private int w;

    public PizzaBox(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 50;
    }

    public int getW() {
        return 50;
    }

}
